import{html,css,LitElement} from'lit-element';
import'./LitDatos.js'

export class LitTabla extends LitElement{
    constructor(){
        super();
        this.m=[];
        this.addEventListener('ApiData',(e)=>{
          this.__dataFormat(e.detail.data.results);
        })
    }
    static get properties(){
      return{
          m:{type:String}
      };
  }
    __dataFormat(data){
let d=[];
let stations=[];
let indexes=[];
let measurements=[];
data.forEach(s=>{
  s.stations.forEach(st=>{
    stations.push({
      id:st.id,
      name:st.name, 
      calculationTi:st.calculationTi,
    })
  })
});

data.forEach(st=>{
  st.stations.forEach(i=>{
    indexes.push({
      calculationTime:i.calculationTime,
      responsiblePollumant:i.responsiblePollumant,
      value:i.value,
      scale: i.scale
    })
  })
});

data.forEach(s=>{
  s.stations.forEach(st=>{
    st.measurements.forEach(m=>{
      measurements.push({
        averagedOverInHours:m.averagedOverInHours,
        time:m.time,
        value2:m.value2,
        unit:m.unit,
        pollutant:m.pollutant
      })
    })
  })
});

  this.m=stations;
  console.log(this.m)
    }



    get dataTable(){
        return html`
        
          <table>
    <thead>
      <tr>
        <th>name</th>
        <th>id</th>
        <th>calculationTime</th>
        <th>responsiblePollumant</th>
        <th>Valor</th>
        <th>averagedOverInHours</th>
        <th>time</th>
        <th>unidad</th>
        <th>pollutant</th>
      </tr>
    </thead>
    <tbody>
    ${this.m.map(d=> html`
          <tr>
            <td>${d.name}</td>
            <td>${d.id}</td>
            <td>${d.calculationTime}</td>
            <td>${d.responsiblePollumant}</td>
            <td>${d.value}</td>
            <td>${d.averagedOverInHours}</td>
            <td>${d.time}</td>
            <td>${d.unit}</td>
            <td>${d.pollutant}</td>
          </tr>`)}
    </tbody>
  </table>`
    }

    render(){
      return html`
      <lit-datos url="https://api.datos.gob.mx/v1/calidadAire" method="GET"></lit-datos>
      ${this.dataTable}
      `
    }
}